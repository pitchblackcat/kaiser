import {createServer, Server} from 'http';
import * as express from 'express';
import * as socketIo from 'socket.io';
import {Inject, registerService} from "kaiser-lib";
import {Laundromat} from "./sock/Laundromat";

export class GameServer {
    public static readonly PORT: number = 1337;
    private app: express.Application;
    private server: Server;
    private port: string | number;
    private io: socketIo.Server;

    @Inject private laundromat: Laundromat;

    constructor() {
        this.createApp();
        this.config();
        this.createServer();
        this.sockets();
        this.listen();
    }

    public getApp(): express.Application {
        return this.app;
    }

    private createApp(): void {
        this.app = express();
    }

    private createServer(): void {
        this.server = createServer(this.app);
    }

    private config(): void {
        this.port = GameServer.PORT;
    }

    private sockets(): void {
        this.io = socketIo(this.server);
        registerService(this.io, {name: 'io'});
    }

    private listen() {
        this.server.listen(this.port, () => {
            console.log('Running server on port %s', this.port);
        });
        this.laundromat.start();
    }
}