import {Socket} from "socket.io";

export abstract class AbstractSock {
    public abstract OnConnect(socket: Socket);
    public abstract OnDisconnect(socket: Socket);
}