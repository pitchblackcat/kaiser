import {AbstractSock} from "./AbstractSock";
import {Service, Inject} from "kaiser-lib";
import * as socketIo from "socket.io";
import {Socket} from "socket.io";
import SocketIO = require("socket.io");
import {UserStore} from "../store/UserStore";

@Service({container: 'socks'})
export class UserSock extends AbstractSock {
    @Inject private io: socketIo.Server;
    @Inject private userStore: UserStore;

    OnConnect(socket: SocketIO.Socket) {
        this.welcome(socket);

        socket.on('username', (name: string) => {
            console.log(`${this.userStore.get(socket)} changed name to ${name}`);
            this.userStore.set(socket, name);
            this.emitWhichUsers();
        });

        socket.on('which_users',  () => {
            this.emitWhichUsers(socket);
        });
    }

    private welcome(socket: Socket) {
        const name = this.userStore.set(socket);
        socket.emit('welcome', name);
        console.log(`${name} connected.`);
        this.emitWhichUsers();
    }

    OnDisconnect(socket: SocketIO.Socket) {
        console.log(`${this.userStore.get(socket)} disconnected.`);
        this.userStore.remove(socket);
        socket.disconnect()
    }

    private emitWhichUsers(socket: any = null) {
        let s: any = socket == null ? this.io : socket;
        s.emit('which_users', this.userStore.all());
    }
}