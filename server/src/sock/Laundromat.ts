import {Service, InjectAll, Inject} from 'kaiser-lib';
import {Server} from 'http';
import * as socketIo from "socket.io";
import {Socket} from "socket.io";
import {AbstractSock} from "./AbstractSock";

@Service()
export class Laundromat {
    @Inject private io: socketIo.Server;
    @InjectAll({container: 'socks'}) private socks: AbstractSock[];

    start() {
        this.io.on('connection', (socket: Socket) => {
            for (const sock of this.socks) {
                sock.OnConnect(socket);
            }

            socket.on('disconnect', () => {
                for (const sock of this.socks) {
                    sock.OnDisconnect(socket);
                }
            });
        });
    }
}