import {AbstractSock} from "./AbstractSock";
import {Service, Inject} from "kaiser-lib";
import * as socketIo from "socket.io";
import {Socket} from "socket.io";
import SocketIO = require("socket.io");
import {UserStore} from "../store/UserStore";

@Service({container: 'socks'})
export class MessageSock extends AbstractSock {
    @Inject private io: socketIo.Server;
    @Inject private userStore: UserStore;

    OnConnect(socket: SocketIO.Socket) {
        socket.on('message', (message) => {
            const name = this.userStore.get(socket);
            console.log(`${name}: ${message}`);
            this.io.emit('message', {user: name, msg: message});
        });
    }

    OnDisconnect(socket: SocketIO.Socket) {
    }
}