import {Service} from "kaiser-lib";
import {Socket} from "socket.io";
import SocketIO = require("socket.io");

@Service()
export class UserStore {
    private users = {};

    set(socket: Socket, name: string = null) {
        this.users[socket.id] = name || `Guest ${socket.id}`;
        return this.get(socket);
    }

    remove(socket: SocketIO.Socket) {
        delete this.users[socket.id];
    }

    get(socket: Socket) {
        return this.users[socket.id];
    }

    all() {
        return Object.keys(this.users)
            .map(k => this.users[k])
    }
}