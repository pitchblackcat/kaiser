import {Logger} from "kaiser-lib";

Logger.configure({
    groups: []
});

import {GameServer} from "./GameServer";
import {UserSock} from "./sock/UserSock";
import {MessageSock} from "./sock/MessageSock";

const bootstrap = [
    UserSock,
    MessageSock
];

let app = new GameServer().getApp();
export { app };