<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>3</int>
        <key>texturePackerVersion</key>
        <string>3.6.0</string>
        <key>fileName</key>
        <string>E:/www/awesome/kaiser/sprites.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>premultiplyAlpha</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>easeljs</string>
        <key>textureFileName</key>
        <filename>client/assets/sprites/tiles.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>2</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string>assets/sprites/</string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>2</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>reduceBorderArtifacts</key>
        <true/>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>forceWordAligned</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>assets/sprites/tiles.json</filename>
            </struct>
            <key>json</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>client/assets/sprites/tiles.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>cleanTransparentPixels</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>innerPadding</key>
            <uint>0</uint>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>heuristicMask</key>
            <true/>
            <key>pivotPoint</key>
            <enum type="SpriteSettings::PivotPoint">Center</enum>
        </struct>
        <key>fileList</key>
        <array>
            <filename>textures/hexagonTiles/Tiles/alienBeige.png</filename>
            <filename>textures/hexagonTiles/Tiles/alienBlue.png</filename>
            <filename>textures/hexagonTiles/Tiles/alienGreen.png</filename>
            <filename>textures/hexagonTiles/Tiles/alienPink.png</filename>
            <filename>textures/hexagonTiles/Tiles/alienYellow.png</filename>
            <filename>textures/hexagonTiles/Tiles/bushAutumn.png</filename>
            <filename>textures/hexagonTiles/Tiles/bushDirt.png</filename>
            <filename>textures/hexagonTiles/Tiles/bushGrass.png</filename>
            <filename>textures/hexagonTiles/Tiles/bushMagic.png</filename>
            <filename>textures/hexagonTiles/Tiles/bushSand.png</filename>
            <filename>textures/hexagonTiles/Tiles/bushSnow.png</filename>
            <filename>textures/hexagonTiles/Tiles/flowerBlue.png</filename>
            <filename>textures/hexagonTiles/Tiles/flowerGreen.png</filename>
            <filename>textures/hexagonTiles/Tiles/flowerRed.png</filename>
            <filename>textures/hexagonTiles/Tiles/flowerWhite.png</filename>
            <filename>textures/hexagonTiles/Tiles/flowerYellow.png</filename>
            <filename>textures/hexagonTiles/Tiles/hillAutumn.png</filename>
            <filename>textures/hexagonTiles/Tiles/hillDirt.png</filename>
            <filename>textures/hexagonTiles/Tiles/hillGrass.png</filename>
            <filename>textures/hexagonTiles/Tiles/hillMagic.png</filename>
            <filename>textures/hexagonTiles/Tiles/hillSand.png</filename>
            <filename>textures/hexagonTiles/Tiles/hillSnow.png</filename>
            <filename>textures/hexagonTiles/Tiles/pineAutumn_high.png</filename>
            <filename>textures/hexagonTiles/Tiles/pineAutumn_low.png</filename>
            <filename>textures/hexagonTiles/Tiles/pineAutumn_mid.png</filename>
            <filename>textures/hexagonTiles/Tiles/pineBlue_high.png</filename>
            <filename>textures/hexagonTiles/Tiles/pineBlue_low.png</filename>
            <filename>textures/hexagonTiles/Tiles/pineBlue_mid.png</filename>
            <filename>textures/hexagonTiles/Tiles/pineGreen_high.png</filename>
            <filename>textures/hexagonTiles/Tiles/pineGreen_low.png</filename>
            <filename>textures/hexagonTiles/Tiles/pineGreen_mid.png</filename>
            <filename>textures/hexagonTiles/Tiles/rockDirt.png</filename>
            <filename>textures/hexagonTiles/Tiles/rockDirt_moss1.png</filename>
            <filename>textures/hexagonTiles/Tiles/rockDirt_moss2.png</filename>
            <filename>textures/hexagonTiles/Tiles/rockDirt_moss3.png</filename>
            <filename>textures/hexagonTiles/Tiles/rockSnow_1.png</filename>
            <filename>textures/hexagonTiles/Tiles/rockSnow_2.png</filename>
            <filename>textures/hexagonTiles/Tiles/rockSnow_3.png</filename>
            <filename>textures/hexagonTiles/Tiles/rockStone.png</filename>
            <filename>textures/hexagonTiles/Tiles/rockStone_moss1.png</filename>
            <filename>textures/hexagonTiles/Tiles/rockStone_moss2.png</filename>
            <filename>textures/hexagonTiles/Tiles/rockStone_moss3.png</filename>
            <filename>textures/hexagonTiles/Tiles/smallRockDirt.png</filename>
            <filename>textures/hexagonTiles/Tiles/smallRockGrass.png</filename>
            <filename>textures/hexagonTiles/Tiles/smallRockSnow.png</filename>
            <filename>textures/hexagonTiles/Tiles/smallRockStone.png</filename>
            <filename>textures/hexagonTiles/Tiles/tileAutumn.png</filename>
            <filename>textures/hexagonTiles/Tiles/tileDirt.png</filename>
            <filename>textures/hexagonTiles/Tiles/tileGrass.png</filename>
            <filename>textures/hexagonTiles/Tiles/tileLava.png</filename>
            <filename>textures/hexagonTiles/Tiles/tileMagic.png</filename>
            <filename>textures/hexagonTiles/Tiles/tileRock.png</filename>
            <filename>textures/hexagonTiles/Tiles/tileSand.png</filename>
            <filename>textures/hexagonTiles/Tiles/tileSnow.png</filename>
            <filename>textures/hexagonTiles/Tiles/tileStone.png</filename>
            <filename>textures/hexagonTiles/Tiles/tileWater.png</filename>
            <filename>textures/hexagonTiles/Tiles/tileWood_bridge.png</filename>
            <filename>textures/hexagonTiles/Tiles/treeAutumn_high.png</filename>
            <filename>textures/hexagonTiles/Tiles/treeAutumn_low.png</filename>
            <filename>textures/hexagonTiles/Tiles/treeAutumn_mid.png</filename>
            <filename>textures/hexagonTiles/Tiles/treeBlue_high.png</filename>
            <filename>textures/hexagonTiles/Tiles/treeBlue_low.png</filename>
            <filename>textures/hexagonTiles/Tiles/treeBlue_mid.png</filename>
            <filename>textures/hexagonTiles/Tiles/treeCactus_1.png</filename>
            <filename>textures/hexagonTiles/Tiles/treeCactus_2.png</filename>
            <filename>textures/hexagonTiles/Tiles/treeCactus_3.png</filename>
            <filename>textures/hexagonTiles/Tiles/treeGreen_high.png</filename>
            <filename>textures/hexagonTiles/Tiles/treeGreen_low.png</filename>
            <filename>textures/hexagonTiles/Tiles/treeGreen_mid.png</filename>
            <filename>textures/hexagonTiles/Tiles/waveLava.png</filename>
            <filename>textures/hexagonTiles/Tiles/waveWater.png</filename>
            <filename>textures/hexagonBuildings/Tiles/redRoofPointy.png</filename>
            <filename>textures/hexagonBuildings/Tiles/redRoofRounded.png</filename>
            <filename>textures/hexagonBuildings/Tiles/redRoofShort.png</filename>
            <filename>textures/hexagonBuildings/Tiles/redRoofStraight.png</filename>
            <filename>textures/hexagonBuildings/Tiles/redRoofTall.png</filename>
            <filename>textures/hexagonBuildings/Tiles/rockDoorLeft.png</filename>
            <filename>textures/hexagonBuildings/Tiles/rockDoorRight.png</filename>
            <filename>textures/hexagonBuildings/Tiles/rockDoorWindow.png</filename>
            <filename>textures/hexagonBuildings/Tiles/rockDoorWindowBlinds.png</filename>
            <filename>textures/hexagonBuildings/Tiles/rockDoorWindowBlindsMirror.png</filename>
            <filename>textures/hexagonBuildings/Tiles/rockDoorWindowMirror.png</filename>
            <filename>textures/hexagonBuildings/Tiles/rockGateLeft.png</filename>
            <filename>textures/hexagonBuildings/Tiles/rockGateRight.png</filename>
            <filename>textures/hexagonBuildings/Tiles/rockWindowBlinds.png</filename>
            <filename>textures/hexagonBuildings/Tiles/rockWindowBlindsLeft.png</filename>
            <filename>textures/hexagonBuildings/Tiles/rockWindowBlindsRight.png</filename>
            <filename>textures/hexagonBuildings/Tiles/rockWindowLeft.png</filename>
            <filename>textures/hexagonBuildings/Tiles/rockWindowRight.png</filename>
            <filename>textures/hexagonBuildings/Tiles/rockWindows.png</filename>
            <filename>textures/hexagonBuildings/Tiles/sandDoorLeft.png</filename>
            <filename>textures/hexagonBuildings/Tiles/sandDoorRight.png</filename>
            <filename>textures/hexagonBuildings/Tiles/sandDoorWindow.png</filename>
            <filename>textures/hexagonBuildings/Tiles/sandDoorWindowBlinds.png</filename>
            <filename>textures/hexagonBuildings/Tiles/sandDoorWindowBlindsMirror.png</filename>
            <filename>textures/hexagonBuildings/Tiles/sandDoorWindowMirror.png</filename>
            <filename>textures/hexagonBuildings/Tiles/sandGateLeft.png</filename>
            <filename>textures/hexagonBuildings/Tiles/sandGateRight.png</filename>
            <filename>textures/hexagonBuildings/Tiles/sandRoofPointy.png</filename>
            <filename>textures/hexagonBuildings/Tiles/sandRoofRounded.png</filename>
            <filename>textures/hexagonBuildings/Tiles/sandRoofShort.png</filename>
            <filename>textures/hexagonBuildings/Tiles/sandRoofStraight.png</filename>
            <filename>textures/hexagonBuildings/Tiles/sandRoofTall.png</filename>
            <filename>textures/hexagonBuildings/Tiles/sandWindowBlinds.png</filename>
            <filename>textures/hexagonBuildings/Tiles/sandWindowBlindsLeft.png</filename>
            <filename>textures/hexagonBuildings/Tiles/sandWindowBlindsRight.png</filename>
            <filename>textures/hexagonBuildings/Tiles/sandWindowLeft.png</filename>
            <filename>textures/hexagonBuildings/Tiles/sandWindowRight.png</filename>
            <filename>textures/hexagonBuildings/Tiles/sandWindows.png</filename>
            <filename>textures/hexagonBuildings/Tiles/stoneDoorLeft.png</filename>
            <filename>textures/hexagonBuildings/Tiles/stoneDoorRight.png</filename>
            <filename>textures/hexagonBuildings/Tiles/stoneDoorWindow.png</filename>
            <filename>textures/hexagonBuildings/Tiles/stoneDoorWindowBlinds.png</filename>
            <filename>textures/hexagonBuildings/Tiles/stoneDoorWindowBlindsMirror.png</filename>
            <filename>textures/hexagonBuildings/Tiles/stoneDoorWindowMirror.png</filename>
            <filename>textures/hexagonBuildings/Tiles/stoneGateLeft.png</filename>
            <filename>textures/hexagonBuildings/Tiles/stoneGateRight.png</filename>
            <filename>textures/hexagonBuildings/Tiles/stoneRoofPointy.png</filename>
            <filename>textures/hexagonBuildings/Tiles/stoneRoofRounded.png</filename>
            <filename>textures/hexagonBuildings/Tiles/stoneRoofShort.png</filename>
            <filename>textures/hexagonBuildings/Tiles/stoneRoofStraight.png</filename>
            <filename>textures/hexagonBuildings/Tiles/stoneRoofTall.png</filename>
            <filename>textures/hexagonBuildings/Tiles/stoneWindowBlinds.png</filename>
            <filename>textures/hexagonBuildings/Tiles/stoneWindowBlindsLeft.png</filename>
            <filename>textures/hexagonBuildings/Tiles/stoneWindowBlindsRight.png</filename>
            <filename>textures/hexagonBuildings/Tiles/stoneWindowLeft.png</filename>
            <filename>textures/hexagonBuildings/Tiles/stoneWindowRight.png</filename>
            <filename>textures/hexagonBuildings/Tiles/stoneWindows.png</filename>
            <filename>textures/hexagonBuildings/Tiles/woodDoorLeft.png</filename>
            <filename>textures/hexagonBuildings/Tiles/woodDoorRight.png</filename>
            <filename>textures/hexagonBuildings/Tiles/woodDoorWindow.png</filename>
            <filename>textures/hexagonBuildings/Tiles/woodDoorWindowBlinds.png</filename>
            <filename>textures/hexagonBuildings/Tiles/woodDoorWindowBlindsMirror.png</filename>
            <filename>textures/hexagonBuildings/Tiles/woodDoorWindowMirror.png</filename>
            <filename>textures/hexagonBuildings/Tiles/woodGateLeft.png</filename>
            <filename>textures/hexagonBuildings/Tiles/woodGateRight.png</filename>
            <filename>textures/hexagonBuildings/Tiles/woodRoofPointy.png</filename>
            <filename>textures/hexagonBuildings/Tiles/woodRoofRounded.png</filename>
            <filename>textures/hexagonBuildings/Tiles/woodRoofShort.png</filename>
            <filename>textures/hexagonBuildings/Tiles/woodRoofStraight.png</filename>
            <filename>textures/hexagonBuildings/Tiles/woodRoofTall.png</filename>
            <filename>textures/hexagonBuildings/Tiles/woodWindowBlinds.png</filename>
            <filename>textures/hexagonBuildings/Tiles/woodWindowBlindsLeft.png</filename>
            <filename>textures/hexagonBuildings/Tiles/woodWindowBlindsRight.png</filename>
            <filename>textures/hexagonBuildings/Tiles/woodWindowLeft.png</filename>
            <filename>textures/hexagonBuildings/Tiles/woodWindowRight.png</filename>
            <filename>textures/hexagonBuildings/Tiles/woodWindows.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
    </struct>
</data>
