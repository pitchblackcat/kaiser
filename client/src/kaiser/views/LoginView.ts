import {AbstractView} from "../../engine/view/AbstractView";
import {Manifest} from "../../engine/decorator/preloadjs";
import {SocketService} from "../../socket/socket.service";
import {Html} from "../../templating/Html";
import {ViewManager} from "../../engine/ViewManager";
import {LobbyView} from "./LobbyView";
import {Inject} from "kaiser-lib";

@Manifest({
    key: 'game',
    data: [
        {id: 'sheet', src: 'assets/sprites/tiles.json', type: 'spritesheet'},
    ]
})
export class LoginView extends AbstractView {
    private static container: string = '#login-container';
    @Inject private socketService: SocketService;
    @Inject private viewManager: ViewManager;

    onInit(): void {
        this.socketService.once('welcome').subscribe(() => {
            const username = localStorage.getItem('username');
            if (username) {
                this.login(username);
                return;
            }

            Html.show(LoginView.container);

            Html.get(LoginView.container, 'form').one('submit', () => {
                const name = Html.inputOf(LoginView.container, 'input');
                localStorage.setItem('username', name);
                this.login(name);
                return false;
            });
        });
    }

    login(name: string) {
        this.socketService.username = name;
        this.viewManager.next(new LobbyView());
    }

    onTick(): void {
    }

    onDestroy(): void {
        Html.hide(LoginView.container);
    }
}