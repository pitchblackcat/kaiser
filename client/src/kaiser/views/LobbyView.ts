import {AbstractView} from "../../engine/view/AbstractView";
import {Manifest} from "../../engine/decorator/preloadjs";
import {SocketService} from "../../socket/socket.service";
import {Html} from "../../templating/Html";
import {scan} from "rxjs/internal/operators";
import {Inject} from 'kaiser-lib';

@Manifest({
    key: 'game',
    data: [
        {id: 'sheet', src: 'assets/sprites/tiles.json', type: 'spritesheet'},
    ]
})
export class LobbyView extends AbstractView {
    private static container: string = '#lobby-container';
    private sheet: createjs.SpriteSheet;
    @Inject private socketService: SocketService;

    onInit(): void {
        Html.show(LobbyView.container);
        Html.get(LobbyView.container, 'form').on('submit', () => {
            this.socketService.emit('message', Html.inputOf(LobbyView.container, 'input'));
            Html.setInputOf(LobbyView.container, 'input');
            return false;
        });

        this.socketService.poll<string[]>('which_users').subscribe(users => {
            Html.set(LobbyView.container, '#chat-user-list', users.map(this.createUser).join(''));
        });

        this.socketService.on('message')
            .pipe(
                scan((acc, val) => {
                    acc.push(val);
                    return acc.slice(-10);
                }, []),
            )
            .subscribe(
                messages => Html.set(LobbyView.container, '#chat', messages.map(m => this.createMessage(m)).join('')
            ));
    }

    onTick(): void {
    }

    onDestroy(): void {
    }

    private createUser(user: string) {
        return `<li>${user}</li>`;
    }

    private createMessage(options: any) {
        return `<blockquote class="blockquote${options.user == this.socketService.username ? ' text-right' : ''}">
                    <p class="mb-0">${options.msg}</p>
                    <footer class="blockquote-footer">${options.user}</footer>
                </blockquote>`;
    }
}