import {AbstractView} from "../../engine/view/AbstractView";
import {Manifest} from "../../engine/decorator/preloadjs";
import {Preloader} from "../../engine/Preloader";
import {HexGrid, Hex, IHex, HexFloodFill} from "kaiser-lib";
import {GameTile} from "../GameTile";
import {GameTileEvents} from "../GameTileEvents";
import {Subscription} from "rxjs";
import {HexGridView} from "../../grid/HexGridView";

@Manifest({
    key: 'game',
    data: [
        {id: 'sheet', src: 'assets/sprites/tiles.json', type: 'spritesheet'},
    ]
})
export class GameView extends AbstractView {
    private sheet: createjs.SpriteSheet;
    private gridView: HexGridView<GameTile>;

    private grid = new HexGrid<GameTile>();

    onLoaded(data: any): void {
    }

    onDestroy(): void {
    }

    onTick(): void {
    }

    onInit(): void {
        console.log('initGameView');
        this.attachView();
        this.createGrid();
        this.subscribeToEvents();
    }

    private attachView() {
        this.sheet = Preloader.get<createjs.SpriteSheet>('game', 'sheet');
        this.gridView = new HexGridView(this.grid, {
            tw: 64.5,
            th: 64.5,
            regX: 32.25,
            regY: 32.25,
            spritesheet: this.sheet,
        });
        this.addChild(this.gridView);
    }

    private createGrid() {
        const size = 3;
        for (let x = -size; x <= size; x++) {
            for (let y = -size; y <= size; y++) {
                for (let z = -size; z <= size; z++) {
                    const coord: IHex = {x: x, y: y, z: z};

                    if (!Hex.isValid(coord)) continue;
                    this.grid.put(coord, this.createTile(coord))
                }
            }
        }
        this.zsort();
    }

    private zsort() {
        for (const tile of this.grid.values().sort((a, b) => a.y - b.y)) {
            this.removeChild(tile);
            this.addChild(tile);
        }
    }

    private createTile(coords: IHex) {
        return new GameTile(
            coords,
            this.sheet,
            GameTile.TILES[Math.round(Math.random() * (GameTile.TILES.length - 1))]
        );
    }

    private subscribeToEvents() {
        let sub: Subscription;

        let getProvince = tile => HexFloodFill.flood<GameTile>(
            this.grid,
            tile.hex,
            h => h.tileType == tile.tileType
        );

        let raise = tile => {
            createjs.Tween.removeTweens(tile.tile);
            createjs.Tween.get(tile.tile).to({y: -15}, 400, createjs.Ease.cubicOut);
        };

        let lower = tile => {
            createjs.Tween.removeTweens(tile.tile);
            createjs.Tween.get(tile.tile).to({y: 0}, 800, createjs.Ease.cubicInOut);
        };

        sub = GameTileEvents
            .mouse$('mouseover')
            .subscribe(e => {
                getProvince(e.tile).forEach(raise);
            });
        this.subscriptions.push(sub);

        sub = GameTileEvents
            .mouse$('mouseout')
            .subscribe(e => {
                getProvince(e.tile).forEach(lower);
            });
        this.subscriptions.push(sub);

        sub = GameTileEvents
            .mouse$('click')
            .subscribe(e => {
                e.tile.addHouse();
            });
        this.subscriptions.push(sub);
    }
}