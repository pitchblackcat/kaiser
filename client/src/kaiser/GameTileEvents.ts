import {Observable, Subject} from "rxjs";
import {GameTile} from "./GameTile";
import {filter} from "rxjs/operators";

export class GameTileEvents {
    private static _mouse$ = new Subject<TileMouseEvent>();

    public static emitMouse(event: string, tile: GameTile) {
        GameTileEvents._mouse$.next(new TileMouseEvent(event, tile));
    }

    public static mouse$(events: string | string[]): Observable<TileMouseEvent> {
        return this._mouse$.pipe(
            Array.isArray(events)
            ? filter(e => events.indexOf(e.event) >= 0)
            : filter(e => e.event == events)
        );
    }
}

export class TileMouseEvent {
    public readonly event: string;
    public readonly tile: GameTile;

    constructor(event: string, tile: GameTile) {
        this.event = event;
        this.tile = tile;
    }
}