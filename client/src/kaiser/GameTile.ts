import {IHex} from "kaiser-lib";
import {Province} from "./Province";
import {GameTileEvents} from "./GameTileEvents";
import {HexTile} from "../grid/HexTile";

export class GameTile extends HexTile {
    static readonly TILES = [
        'tileGrass',
        'tileLava',
        'tileMagic',
        'tileDirt',
        'tileSnow',
        'tileRock',
        'tileStone',
        'tileSand',
        'tileAutumn',
    ];

    public readonly tile: createjs.Container = new createjs.Container();
    private floor: createjs.Sprite;
    private sheet: createjs.SpriteSheet;

    private province: Province;
    private tween: createjs.Tween;

    private house: createjs.Sprite;
    private roof: createjs.Sprite;

    public get tileType(): string {
        return this.floor.currentAnimation;
    }

    constructor(hexCoordinates: IHex, spriteSheet: createjs.SpriteSheet, frame: string) {
        super(hexCoordinates);
        this.addChild(this.tile);

        this.sheet = spriteSheet;
        this.floor = new createjs.Sprite(this.sheet, frame);
        this.tile.addChild(this.floor);

        this.tile.on('mouseover', () => GameTileEvents.emitMouse('mouseover', this));
        this.tile.on('mouseout', () => GameTileEvents.emitMouse('mouseout', this));
        this.tile.on('click', () => GameTileEvents.emitMouse('click', this));
    }

    addHouse() {
        console.log('hey');

        this.house = new createjs.Sprite(this.sheet, 'stoneDoorWindowBlinds');
        this.roof = new createjs.Sprite(this.sheet, 'stoneRoofShort');

        this.house.regX = this.roof.regX = 32;
        this.house.regY = this.roof.regY = 32;

        this.house.scaleX = this.house.scaleY = .75;
        this.roof.scaleX = this.roof.scaleY = .75;

        this.house.x = this.roof.x = 32;

        this.house.alpha = this.roof.alpha = 0;
        this.house.y = this.roof.y = -80;

        this.tile.addChild(this.house);
        this.tile.addChild(this.roof);

        createjs.Tween.get(this.house).to({alpha: 1}, 600);
        createjs.Tween.get(this.house).to({y: 14}, 1200, createjs.Ease.bounceOut);

        createjs.Tween.get(this.roof).wait(400).to({alpha: 1}, 600);
        createjs.Tween.get(this.roof).wait(400).to({y: 14}, 1200, createjs.Ease.bounceOut);
    }

    removeHouse() {
        this.tile.removeChild(this.house);
        this.tile.removeChild(this.roof);
        this.house = null;
        this.roof = null;
    }
}