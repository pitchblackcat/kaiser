import {HexGridViewOptions} from "./HexGridView";
import {IHex} from "kaiser-lib";

export class HexTile extends createjs.Container {
    public readonly hex: IHex;

    constructor(hex: IHex) {
        super();
        this.hex = hex;
        Object.seal(this.hex);
    }

    private _options: HexGridViewOptions;

    set options(value: HexGridViewOptions) {
        if (this._options != null) return;
        this._options = value;
    }
}