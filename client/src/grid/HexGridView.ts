import {IHex, DictionaryEvent, HexGridEvent, HexGrid} from "kaiser-lib";
import {HexTile} from "./HexTile";

export class HexGridView<T extends HexTile> extends createjs.Container {
    public readonly hexGrid: HexGrid<T>;
    private options: HexGridViewOptions;

    constructor(hexGrid: HexGrid<T>, options: HexGridViewOptions) {
        super();
        this.options = options;
        this.hexGrid = hexGrid;

        this.hexGrid.change$.subscribe(event => {
            switch (event.event) {
                case DictionaryEvent.PUT:
                    this.drawTile(event);
                    break;
                case DictionaryEvent.DELETED:
                    this.removeTile(event);
                    break;
            }
        });
    }

    getX(key: IHex): number {
        return key.x * this.options.tw
            + key.z * (this.options.tw * .5);
    }

    getY(key: IHex): number {
        return key.z * (this.options.tw * .75);
    }

    protected removeTile(event: HexGridEvent<T>) {
        this.removeChild(event.value);
    }

    protected drawTile(event: HexGridEvent<T>) {
        this.removeChild(event.value);
        this.addChild(event.value);

        event.value.x = this.getX(event.key);
        event.value.y = this.getY(event.key);
        event.value.regX = this.options.regX;
        event.value.regY = this.options.regY;
    }
}

export interface HexGridViewOptions {
    tw: number,
    th: number,
    regX: number,
    regY: number,
    spritesheet: createjs.SpriteSheet
}