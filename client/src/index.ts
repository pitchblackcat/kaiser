import {Engine} from "./engine/Engine";

import 'bootstrap';
import '../web/style.scss';
import {LoginView} from "./kaiser/views/LoginView";

(function () {
    const e = new Engine(() => new LoginView());
})();