import * as $ from "jquery";

export class Html {
    static set(container: string, selector: string, html: string) {
        console.log(`${container} ${selector}`, html);
        $(`${container} ${selector}`).html(html);
    }

    static get(container: string, selector: string) {
        return $(`${container} ${selector}`);
    }

    static inputOf(container: string, selector: string, maxLength: number = 128): string {
        return ($(`${container} ${selector}`).val() + '').trim().substr(0, maxLength);
    }

    static setInputOf(container: string, selector: string, str: string = null): void {
        $(`${container} ${selector}`).val(str);
    }

    static show(container: string) {
        $(container).addClass('show');
    }

    static hide(container: string) {
        $(container).removeClass('show');
    }
}