import {fromEvent, Observable, Subject} from "rxjs";
import {map, shareReplay} from "rxjs/operators";
import {Service} from "kaiser-lib";

@Service()
export class Events {
    public readonly resize$: Observable<Size>;
    public readonly tick$: Subject<any>;

    constructor() {
        this.tick$ = new Subject<any>();
        fromEvent(createjs.Ticker, 'tick').subscribe(() => this.tick$.next());

        this.resize$ = fromEvent(window, 'resize').pipe(
            map(() => new Size(
                Math.max(document.getElementById('canvas').getBoundingClientRect().width, 0),
                Math.max(document.getElementById('canvas').getBoundingClientRect().height, 0)
            )),
            shareReplay(1)
        );
    }
}

export class Size {
    public readonly w: number;
    public readonly h: number;
    public readonly cx: number;
    public readonly cy: number;

    constructor(w: number, h: number) {
        this.w = w;
        this.h = h;
        this.cx = w / 2;
        this.cy = h / 2;
    }
}