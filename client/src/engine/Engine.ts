import {take} from "rxjs/operators";
import {ViewManager} from "./ViewManager";
import {SplashView} from "./view/SplashView";
import {Events, Size} from "./Events";
import {AbstractView} from "./view/AbstractView";
import {Inject} from "kaiser-lib";

export class Engine {
    private stage: createjs.Stage;
    private canvas: HTMLCanvasElement;

    @Inject private viewManager: ViewManager;
    @Inject private events: Events;

    constructor(afterSplash: () => AbstractView) {
        this.stage = new createjs.Stage('canvas');
        this.canvas = this.stage.canvas as HTMLCanvasElement;
        this.events.tick$.subscribe(() => this.tick());
        this.events.resize$.subscribe(size => this.resize(size));

        this.events.tick$
            .pipe(take(1))
            .subscribe(() => {
                this.stage.addChild(this.viewManager);
                this.viewManager.x = 0;
                this.viewManager.y = 0;
                this.viewManager.next(new SplashView(afterSplash));

                this.stage.enableMouseOver();
                window.dispatchEvent(new Event('resize'));
            });
    }

    resize(s: Size) {
        this.canvas.width = s.w;
        this.canvas.height = s.h;
        this.tick();
    }

    tick() {
        this.stage.update();
    }
}