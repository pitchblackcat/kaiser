import {AbstractView} from './view/AbstractView';
import {Preloader} from "./Preloader";
import {Events} from "./Events";
import {filter} from "rxjs/operators";
import {Service, Inject} from 'kaiser-lib';

@Service()
export class ViewManager extends createjs.Container {
    @Inject private events: Events;
    private view: AbstractView;

    constructor() {
        super();
        this.events.resize$.subscribe(s => {
            this.x = s.cx;
            this.y = s.cy;

            if (this.view) this.view.onResize(s);
        });

        this.events.tick$
            .pipe(filter(() => this.view != null))
            .subscribe(() => this.view.onTick())
    }

    public next(view: AbstractView) {
        if (this.view) {
            this.removeChild(this.view);
            this.view.destroy();
        }

        this.view = view;
        const manifest = this.view['manifest'];

        if (manifest) {
            const preloader = new Preloader(manifest.key, manifest.data);
            preloader.load().then(() => {
                this.view.onInit();
                this.addChild(this.view);
            });
        } else {
            this.view.onInit();
            this.addChild(this.view);
        }
    }
}