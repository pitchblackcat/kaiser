import {Subscription} from "rxjs";
import {Size} from "../Events";

export abstract class AbstractView extends createjs.Container {

    public subscriptions: Subscription[] = [];
    private data: any;

    public abstract onTick(): void;

    public abstract onInit(): void;

    public onResize(s: Size) {

    }

    public abstract onDestroy(): void;

    public destroy(): void {
        while (this.subscriptions.length) {
            this.subscriptions.pop().unsubscribe();
        }

        this.onDestroy();
    }
}