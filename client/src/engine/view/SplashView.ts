import {AbstractView} from "./AbstractView";
import {Inject} from "kaiser-lib";
import {ViewManager} from "../ViewManager";
import {Manifest} from "../decorator/preloadjs";
import {Preloader} from "../Preloader";
import {Events} from "../Events";

@Manifest({
    key: 'splash',
    data: [
        { id: 'background', src: 'assets/sprites/background.json', type: 'spritesheet' }
    ]
})
export class SplashView extends AbstractView {
    @Inject viewManager: ViewManager;
    @Inject events: Events;

    splash: createjs.Sprite;

    constructor(private afterSplash: () => AbstractView) {
        super();
    }

    onLoaded(data: any): void {

    }

    onDestroy(): void {

    }

    onTick(): void {

    }

    onInit(): void {
        const sheet = Preloader.get<createjs.SpriteSheet>('splash', 'background');
        this.splash = new createjs.Sprite(sheet, 'background');
        this.addChild(this.splash);
        this.splash.regX = this.splash.getBounds().width / 2;
        this.splash.regY = this.splash.getBounds().height / 2;

        const sub = this.events.resize$.subscribe(s => {
            this.x = s.cx;
            this.y = s.cy;

            this.splash.setTransform(0,0,s.w, s.h);
        });
        this.subscriptions.push(sub);

        setTimeout(
            () => this.viewManager.next(this.afterSplash()),
            100
        );
    }
}