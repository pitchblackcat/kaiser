import {fromEvent, Observable} from 'rxjs';
import {map, take, takeUntil, tap} from 'rxjs/operators';
import {Logger} from 'kaiser-lib';


export class Preloader {
    private static queues: { [key: string]: createjs.LoadQueue } = {};

    public readonly progress$: Observable<number>;
    public readonly error$: Observable<any>;
    public readonly load$: Observable<any>;
    private queue: createjs.LoadQueue;
    private _loaded = false;

    constructor(key: string, manifest: any) {
        this.queue = new createjs.LoadQueue();
        this.queue.loadManifest(manifest);

        this.load$ = fromEvent(this.queue, 'complete').pipe(
            tap(() => Preloader.queues[key] = this.queue),
            map(event => this.queue.getItems(true)),
            take(1)
        );

        this.progress$ = fromEvent(this.queue, 'progress').pipe(
            map(() => (this.queue.progress * 100 | 0)),
            takeUntil(this.load$)
        );

        this.error$ = fromEvent(this.queue, 'error').pipe(
            tap(err => Logger.error(Logger.MANIFEST, err)),
            takeUntil(this.load$)
        );
    }

    static get<T>(manifest: string, key: string): T {
        return <T>Preloader.queues[manifest].getResult(key);
    }

    load(): Promise<any> {
        if (!this._loaded) {
            this.queue.load();
            this._loaded = true;
            return this.load$.toPromise();
        }

        return Promise.resolve(1);
    }
}