export function OnTick() {
    return function(target: any) {
        // save a reference to the original constructor
        const original = target;

        // the new constructor behaviour
        const f: any = function (...args: any[]) {
            console.log('ClassWrapper: before class constructor', original.name);
            let instance = original.apply(this, args);
            console.log('ClassWrapper: after class constructor', original.name);
            return instance;
        };

        // copy prototype so intanceof operator still works
        f.prototype = original.prototype;

        // return new constructor (will override original)
        return f;
    };
}