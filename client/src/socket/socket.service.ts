import { Observable, Observer } from 'rxjs';

import * as io from 'socket.io-client';
import {take} from "rxjs/internal/operators";
import {Service} from 'kaiser-lib';

const SERVER_URL = 'http://localhost:1337';

@Service()
export class SocketService {
    private _username: string;

    get username(): string {
        return this._username;
    }

    set username(value: string) {
        this._username = value.trim().substr(0,32);
        this.emit('username', this._username);
    }
    private socket: io;
    private observables$: {[key: string]: Observable<any>} = {};

    constructor() {
        this.socket = io(SERVER_URL);
        this.once<string>('welcome').subscribe(name => {
            this._username = name;
        });
    }

    public on<T>(event: string): Observable<T> {
        if (Object.keys(this.observables$).indexOf(event) < 0) {
            this.observables$[event] = new Observable<T>(observer => {
                this.socket.on(event, (data: T) => observer.next(data))
            });
        }

        return this.observables$[event];
    }

    public once<T>(event: string): Observable<T> {
        return <Observable<T>> this.on(event).pipe(take(1));
    }

    public ask<T>(event: string): Observable<T> {
        this.emit(event);
        return <Observable<T>> this.once(event);
    }

    public poll<T>(event: string): Observable<T> {
        this.emit(event);
        return <Observable<T>> this.on(event);
    }

    public emit(event: string, data: any = null) {
        this.socket.emit(event, data);
    }

    public onMessage(): Observable<any> {
        return new Observable<any>(observer => {
            this.socket.on('message', (data: any) => observer.next(data));
        });
    }

    public onEvent(event: Event): Observable<any> {
        return new Observable<Event>(observer => {
            this.socket.on(event, () => observer.next());
        });
    }
}