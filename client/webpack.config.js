const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackIncludeAssetsPlugin = require('html-webpack-include-assets-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const WriteFilePlugin = require('write-file-webpack-plugin');

module.exports = {
    devtool: 'source-map',
    mode: 'development',
    node: {
        fs: 'empty'
    },
    entry: {
        main: './src/index.ts'
    },
    output: {
        filename: '[name].[hash].bundle.js',
        path: path.resolve(__dirname, './dist')
    },
    plugins: [
        new WriteFilePlugin(),
        new CleanWebpackPlugin(['./dist'], {allowExternal: true}),
        new CopyWebpackPlugin([
            { from: 'node_modules/createjs/builds/1.0.0/createjs.js', to: 'js/'},
            { from: 'web/*.css', to: 'css/', flatten: true},
            { from: 'assets', to: 'assets' }
        ]),
        new HtmlWebpackPlugin({
            title: 'Loading...',
            filename: 'index.html',
            template: 'web/index.html',
            meta: {
                'viewport': 'width=device-width, initial-scale=1, shrink-to-fit=no',
                'theme-color': '#4285f4'
            }
        }),
        new HtmlWebpackIncludeAssetsPlugin({
            assets: [
                'js/createjs.js',
                'css/style.css'
            ],
            append: false
        }),
        new HtmlWebpackIncludeAssetsPlugin({
            assets: [

            ],
            append: true
        })
    ],
    resolve: {
        // Add `.ts` and `.tsx` as a resolvable extension.
        extensions: [".ts", ".tsx", ".js"]
    },
    module: {
        rules: [
            // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
            { test: /\.tsx?$/, loader: "ts-loader" },
            {
                test: /\.(scss)$/,
                use: [
                    {
                        // Adds CSS to the DOM by injecting a `<style>` tag
                        loader: 'style-loader'
                    },
                    {
                        // Interprets `@import` and `url()` like `import/require()` and will resolve them
                        loader: 'css-loader'
                    },
                    {
                        // Loader for webpack to process CSS with PostCSS
                        loader: 'postcss-loader',
                        options: {
                            plugins: function () {
                                return [
                                    require('autoprefixer')
                                ];
                            }
                        }
                    },
                    {
                        // Loads a SASS/SCSS file and compiles it to CSS
                        loader: 'sass-loader'
                    }
                ]
            }
        ]
    },
    devServer: {
        contentBase: './dist'
    }
};