export class Dictionary<K, V> {
    protected readonly data: { [key: string]: V } = {};

    public has(key: K | string): boolean {
        const hash = this.hash(key);
        return this.data.hasOwnProperty(hash)
    }

    public put(key: K | string, value: V) {
        const hash = this.hash(key);
        this.data[hash] = value;
    }

    public get(key: K | string): V {
        const hash = this.hash(key);
        return this.data.hasOwnProperty(hash) ? this.data[hash] : null;
    }

    public delete(key: K | string) {
        const hash = this.hash(key);
        delete this.data[hash];
    }

    public keys() {
        return Object.getOwnPropertyNames(this.data);
    }

    public values() {
        return this.keys().map(str => this.data[str]);
    }

    public pairs() {
        return this.keys().map(
            key => ({key: key, value: this.data[key]})
        );
    }

    public hash(key: K | string): string {
        return typeof key == 'string'
            ? key
            : JSON.stringify(key, Object.keys(key).sort());
    }
}