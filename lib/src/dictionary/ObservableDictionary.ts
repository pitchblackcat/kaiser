import {Dictionary} from "./Dictionary";
import {Observable, Subject} from "rxjs";
import {IDestroy} from "../interfaces";

export class ObservableDictionary<K, V> extends Dictionary<K, V> implements IDestroy {
    protected _change$: Subject<IDictionaryEvent<K, V>> = new Subject();
    public get change$(): Observable<IDictionaryEvent<K, V>> {
        return this._change$;
    }

    public put(key: K, value: V): void {
        const hash = this.hash(key);
        this.data[hash] = value;
        this.publish(DictionaryEvent.PUT, hash, key, value);
    }

    public delete(key: K): void {
        const hash = this.hash(key);
        const value = this.data[hash];
        if (value) {
            delete this.data[hash];
            this.publish(DictionaryEvent.DELETED, hash, key, value);
        }
    }

    destroy(): void {
        for (const prop of Object.getOwnPropertyNames(this.data)) {
            delete this.data[prop];
        }

        this._change$.complete();
        this._change$ = null;
    }

    protected publish(event: string, hash: string, key: K, value: V) {
        this._change$.next({event: event, hash: hash, key: key, value: value});
    }
}

export class DictionaryEvent {
    static readonly PUT = 'PUT';
    static readonly DELETED = 'DELETED';
}

export interface IDictionaryEvent<K, V> {
    key: K,
    value: V,
    hash: string,
    event: string
}