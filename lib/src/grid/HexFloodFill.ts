import {IHex} from "./Hex";
import {HexGrid} from "./HexGrid";

export class HexFloodFill {
    public static flood<T>(grid: HexGrid<T>, hex: IHex, filter: (T) => boolean) {
        let discovered: string[] = [grid.hash(hex)];
        let levels: IHex[][] = [[hex]];
        let level = 0;

        let notIn = (arr: any[], i) => arr.indexOf(i) < 0;

        let neighboursOf = (hex) => grid
            .neighboursOf(hex)
            .filter(h => notIn(discovered, grid.hash(h)))
            .filter(h => filter(grid.get(h)));

        do {
            for (const h of levels[level]) {
                let n = neighboursOf(h);
                discovered.push(...n.map(grid.hash));
                if (n.length) {
                    if (levels.length == level + 1)
                        levels[level + 1] = [];

                    levels[level + 1].push(...n);
                }
            }
            level++;
        } while (level < levels.length && level < 100);

        return [].concat(...levels).map(h => grid.get(h));
    }
}