export interface IHex {
    x: number,
    y: number,
    z: number
}

export class Hex {
    protected static readonly NEIGHBOURS = [
        {x: 0, y: 1, z: -1},
        {x: 1, y: 0, z: -1},
        {x: 1, y: -1, z: 0},
        {x: 0, y: -1, z: 1},
        {x: -1, y: 0, z: 1},
        {x: -1, y: 1, z: 0},
    ];

    public static of(x: number, y: number, z: number): IHex {
        return {x: x, y: y, z: z};
    }

    public static neighboursOf(hex: IHex): IHex[] {
        return Hex.NEIGHBOURS.map(n => Hex.add(hex, n));
    }

    public static add(a: IHex, b: IHex): IHex {
        return {
            x: a.x + b.x,
            y: a.y + b.y,
            z: a.z + b.z
        };
    }

    public static isValid(v: IHex) : boolean {
        return v.x + v.y + v.z == 0;
    }
}