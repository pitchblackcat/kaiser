import {Subject} from 'rxjs';
import {IDictionaryEvent, ObservableDictionary} from "../dictionary/ObservableDictionary";
import {Hex, IHex} from "./Hex";

export class HexGrid<T> extends ObservableDictionary<IHex, T> {
    constructor() {
        super();
        this.reset() ;
    }

    reset() {
        this.destroy();
        this._change$ = new Subject<IDictionaryEvent<IHex, T>>();
    }

    neighboursOf(hex: IHex): IHex[] {
        return Hex.neighboursOf(hex).filter(h => this.has(h));
    }

    public floodFill(start: T, target: T = null, filter: (T) => boolean = null) {
        console.log('something') ;
    }
}

export interface HexGridEvent<V> {
    key: IHex,
    value: V,
    hash: string,
    event: string,
}