import {Logger} from "../logger/Logger";

export interface IocOptions {
    eager?: boolean,
    name?: string,
    container?: string,
}

export interface IocInjectOptions {
    container?: string
}

class IocContainer {
    private factory: { [key: string]: Function } = {};
    private services: { [key: string]: any } = {};

    register(key: string, func: Function, options: IocOptions) {
        if (options.eager) {
            this.services[key] = func();
        } else {
            this.factory[key] = func;
        }
    }

    registerNow(key: string, service: any) {
        this.services[key] = service;
    }

    get(key: string): any {
        return this.services[key] || this.create(key);
    }

    getAll(): any {
        return [...Object.keys(this.factory), ...Object.keys(this.services)]
            .map(key => this.get(key));
    }

    private create(key: string): any {
        Logger.log(Logger.IOC, `Created '${key}'`);
        this.services[key] = this.factory[key]();
        delete this.factory[key];
        return this.services[key];
    }
}

const _containers: IocContainer[] = [];
function container(name: string = '_default') {
    if (!_containers[name]) {
        _containers[name] = new IocContainer();
    }
    return _containers[name];
}

export function Service<T extends { new(...args: any[]): {} }>(options: IocOptions = {}) {
    return function (target: { new(...args: any[]): {} }) {
        container(options.container).register(options.name || target.name, () => new target(), options);
        Logger.log(Logger.IOC, `Discovered '${target.name}'`);
    };
}

export function registerService<T>(service:T, options: IocOptions) {
    options.name = options.name.replace(/^\w/, c => c.toUpperCase());
    container(options.container).registerNow(options.name, service);
    Logger.log(Logger.IOC, `Discovered '${options.name}'`);
}

export function Inject(target: any, key: string) {
    if (delete target[key]) {
        Logger.log(Logger.IOC, `Autowired ${target.constructor.name}.${key}`);
        const clazz = key.replace(/^\w/, c => c.toUpperCase());

        Object.defineProperty(target, key, {
            get: () => _containers['_default'].get(clazz),
            enumerable: true,
            configurable: false
        });
    }
}

export function InjectAll(options: IocInjectOptions) {
    return (target: any, key: string) => {
        if (delete target[key]) {
            Logger.log(Logger.IOC, `Autowired all ${target.constructor.name}.${options.container}`);

            Object.defineProperty(target, key, {
                get: () => container(options.container).getAll(),
                enumerable: true,
                configurable: false
            });
        }
    };
}